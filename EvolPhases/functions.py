import math
import json
import pandas as pd
import numpy as np

def add_Teff(df):
    sigma = 5.67e-8
    df['EFFECTIVE_TEMPERATURE'] = df.apply(lambda row: math.pow(row['LUMINOSITY']*3.8e26/(4*math.pi*sigma*math.pow(row['RADIUS']*6.96e8,2)),0.25),axis=1)
    return df

def remove_incomplete_tracks_GB(df,Xc_TAMS=1e-6):

    for M in df['MASS'].unique():
        df_mass = df[df['MASS']==M]
        for Mi in df_mass['INITIAL_MASS'].unique():
            if df_mass[df_mass['INITIAL_MASS']==Mi]['HELIUM_IGNITED_FLAG'].max()!=1:
                df = df.drop(df[(df['MASS']==M) & (df['INITIAL_MASS']==Mi)].index)
            # elif df_mass[df_mass['INITIAL_MASS']==Mi]['CENTRAL_HYDROGEN'].max()<Xc_TAMS/10:
            #     df = df.drop(df[(df['MASS']==M) & (df['INITIAL_MASS']==Mi)].index)
                
    return df

def calc_time_proxy_resolution(
        df,
        tp_name = 'CENTRAL_DEGENERACY',
        q_name = 'LOG_RADIUS',
        delta_target = 0.05,
        ):
    
    df_dq_dtp = pd.DataFrame(columns = np.sort(df[tp_name].unique()))
    for M in df['MASS'].unique()[:]:
        df_M = df[df['MASS']==M]
        for Mi in df_M['INITIAL_MASS'].unique():
            track = df_M[df_M['INITIAL_MASS']==Mi]
            track.drop_duplicates([tp_name],inplace=True)
            dq_dtp = np.abs(np.gradient(track[q_name],track[tp_name]))
            series_to_add = pd.Series(dq_dtp, index=list(track[tp_name].values))
            df_dq_dtp = df_dq_dtp.append(series_to_add, ignore_index=True)


    df_dq_dtp.loc['deta'] = delta_target/np.nanmax(df_dq_dtp,axis=0).astype(float)
    allowed_deta = df_dq_dtp.loc['deta'].rolling(window=10).mean().dropna().sort_index()

    return allowed_deta


def get_degen_targets(evol_phase, allowed_deta, first_target, max_allowed_deta = 0.5):

    # INTEGRATE DEGENERACY ARRAY
    degen_targets = [first_target]
    while degen_targets[-1] < allowed_deta.index.max():
        deta = allowed_deta.values[np.argmin(abs(allowed_deta.index-degen_targets[-1]))]
        deta = min(deta,max_allowed_deta)
        degen_targets += [round(degen_targets[-1]+deta,6)]

    print(len(degen_targets))
    if len(degen_targets) != len(set(degen_targets)):
        raise ValueError('DUPLICATE DEGEN TARGETS')
    
    save_targets_file_path = f'/Users/natalierees/mint_general_interpolation_grid_builder/MINT/config/time_proxy_targets/{evol_phase}.py'
    with open(save_targets_file_path, 'w') as f:
        f.write('targets = ')
        json.dump(degen_targets, f, indent=2) 
    
    return degen_targets

def calc_deta(x,q_name='LOG_RADIUS',delta_target = 0.05):
    if len(x) > 1:
        return delta_target/np.abs(np.gradient(x[q_name],x['HELIUM_CORE_MASS_FRACTION']))

def calc_core_mass_resolution(
        df,
        tp_name = 'CENTRAL_DEGENERACY',
        q_name = 'LOG_RADIUS',
        delta_target = 0.05,
        quantile = 0.1
        ):
    allowed_deta = df.groupby(['MASS',tp_name]).apply(lambda x: calc_deta(x,q_name,delta_target))
    allowed_deta = pd.DataFrame(allowed_deta,columns=['Values'])['Values'].apply(pd.Series).quantile(quantile)
    allowed_deta.index = df['HELIUM_CORE_MASS_FRACTION'].unique()
    return allowed_deta.rolling(window=10).mean().dropna().sort_index()