#!/usr/bin/env perl
use Carp qw(confess);
use 5.10.0;
use PerlIO::gzip;

$|=1;
use strict;
# script to test rob's grids to make SURE them are rectangular!

# args are: filename n
# where n is the number of parameters

my $format='%g';
my $f=$ARGV[0];
my $n=$ARGV[1];
if(($f eq '')||($n eq ''))
{
    print "usage: test_for_rectangular_table.pl filename n\n";
    exit;
}

my @min;
my @max;
my @skip;
my @l;
my $lc;
my @prevl;
my @skipcount;
my $warn=0;
my @grid_values;
my @grid_array;
my $errcount=0;
my $maxerrcount=10;
for(my $i=0;$i<$n;$i++)
{
    $skipcount[$i]=0;
    $max[$i]=-1e200;
    $min[$i]=1e200;
}

print "First parse:\n";
open(FP,'<:gzip(autopop)',$f)||confess("cannot open $f");

my $lc=0;
while(<FP>)
{
    # clean up (e.g. remove C code)
    cleanline();

    # skip some lines (comments etc)
    next if(skipline($_));

    # split line on space or commas or some combination of the two
    @l=split(/\s*[\s,]\s*/o);

    # skip short lines
    next if ($#l<1);

    # all lines should have the same number of items
    state $nd;
    if(!defined $nd)
    {
        $nd = scalar @l;
        print "Data line $lc has $n variables, ",$nd-$n," data items\n";
    }

    if(scalar @l != $nd)
    {
        print "Data line $lc has wrong number of items (",scalar @l,") compared to a previous line ($nd)! \n";
    }

    # loop over data
    for(my $i=0;$i<$n;$i++)
    {
        # standard format
        $l[$i] = sprintf $format,$l[$i];

        # save max/min
        $max[$i]=$l[$i] if($l[$i]>$max[$i]);
        $min[$i]=$l[$i] if($l[$i]<$min[$i]);

        # save co-ordinates in grid_values hash
        $grid_values[$i]->{$l[$i]}=1;
    }
    $lc++;
}

print "grid limits: \nMIN @min\nMAX @max\n";
print "Line count $lc\n";
my @pos;
my @posmax;
$lc=0;

for(my $i=0;$i<$n;$i++)
{
    $pos[$i]=0;

    # get list of co-ordinate values
    my @g=sort {$a<=>$b} keys %{$grid_values[$i]};
    print "Variable $i has ",$#g+1," grid values: @g\n";

    # save number
    $posmax[$i]=$#g;

    # set what we think the grid array should be
    for(my $j=0;$j<=$posmax[$i];$j++)
    {
        $grid_array[$i][$j]=$g[$j];
    }
}

my @x;
print "Second parse:\n";
print "posmax @posmax\n";

# NB you cannot use seek on FP, so reopen
close FP;
open(FP,'<:gzip(autopop)',$f)||confess("cannot open $f");
while(<FP>)
{
    cleanline();
    next if(skipline($_));

    $lc++;
    #print "line $lc : pos @pos\n";
    for(my $i=0;$i<$n;$i++)
    {
        $x[$i]=$grid_array[$i][$pos[$i]];
    }
    chomp;

    # cleanup
    s/\#define \S+ //;
    s/,\\$//o;s/^\s+//o;
    s!/\*.*\*/!!o;
    @l=split(/\s*[\s,]\s*/o);

    map
    {
        $_=sprintf $format,$_;
    }@l;

    next if ($#l<1);

    @l=splice(@l,0,$n);
    @prevl=@l if($#prevl==-1);

    if("@x" ne "@l")
    {
        print "Line $lc should be @x but is @l\n";
        $errcount++;
    }

    for(my $i=0;$i<$n;$i++)
    {
        if($l[$i] ne $prevl[$i])
        {
            #print "$i (prev $prevl[$i] now $l[$i]) changes after $skipcount[$i]\n";
            if($skip[$i] eq '')
            {
                $skip[$i]=$skipcount[$i];
            }
            elsif($skip[$i] ne $skipcount[$i])
            {
                print "line $lc : WARNING: inconsistent skip count for variable $i! Was $skip[$i] this count $skipcount[$i]\n";
                $warn++;
            }
            $skipcount[$i]=1;
        }
        else
        {
            $skipcount[$i]++;
        }
    }
    @prevl=@l;

    for(my $j=$n-1;$j>=0;$j--)
    {
        if(++$pos[$j]>$posmax[$j])
        {
            $pos[$j]=0;
        }
        else
        {
            last;
        }
    }

    if($errcount > $maxerrcount)
    {
        print "Too many errors (maxerrcount=$maxerrcount)\n";
        last;
    }
}
close FP;
for(my $i=0;$i<$n;$i++)
{
    print "Variable $i : Skip count = $skipcount[$i]\n";
}

if($errcount || $warn)
{
    print "There were warnings/errors! Please take heed and see above\n";
}
else
{
    print "There were no warnings or errors, grid seems fine\n";
}

sub skipline
{
    return
        $_[0]=~/\#define\s+\S+\s+\d+\s*$/ ||
        $_[0]=~/^\s+$/o ||
        $_[0]=~/^\#/o;
}

sub cleanline
{
    chomp;
    s!//.*!!o;
    s/^\s*\#define\s*\S+\s*//;
    s!/\*.*\*/!!o;
    s/,\s*\\\s*$//o;s/^\s+//o;
}
